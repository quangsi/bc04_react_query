/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        "color-0": "#0f4c5c",
        "color-1": "#e36414",
        "color-2": "#fb8b24",
        "color-3": "#5f0f40",
      },
    },
  },
  plugins: [],
};
// let bgArr = ["#0f4c5c", "#e36414", "#fb8b24", "#5f0f40"];
