import logo from "./logo.svg";
import "./App.css";
import "antd/dist/antd.css";
import { Input, List, Typography } from "antd";
import { useMutation, useQueries, useQuery, useQueryClient } from "react-query";
import { addTodo, getTodos } from "./service/api";
import { useState } from "react";

function App() {
  const [todoTitle, setTodoTitle] = useState("");
  const queryClient = useQueryClient();
  // data: todos => rename
  let {
    isLoading,
    data: todos,
    isError,
    isFetching,
  } = useQuery("todos", getTodos);
  console.log("isError: ", isError);

  const addTodoMutation = useMutation(addTodo, {
    onSuccess: () => {
      // khi add thành công thì làm mất chức năng cache của api getTodos ( gọi lại api )
      queryClient.invalidateQueries("todos");
    },
  });

  let handleOnChange = (e) => {
    let value = e.target.value;
    setTodoTitle(value);
    console.log("value: ", value);
  };

  let handleAddTodo = () => {
    let todo = {
      title: todoTitle,
      isCompleted: false,
    };
    addTodoMutation.mutate(todo);
  };

  let renderContent = () => {
    if (isFetching) {
      return <h2 className="text-center">Loading ...</h2>;
    }
    if (isError) {
      return <h2 className="text-center">Not available</h2>;
    }
    return (
      <List
        header={<div>Todo List</div>}
        footer={<div></div>}
        bordered
        dataSource={todos}
        renderItem={(item, index) => {
          let number = index % 4;

          let bg_class = `bg-color-${number}`;

          return (
            <List.Item className={bg_class}>
              <Typography.Text mark>[ITEM]</Typography.Text>
              <p className=" text-white">{item.title}</p>
            </List.Item>
          );
        }}
      />
    );
  };
  return (
    <div className="container mx-auto py-5 space-y-5">
      <Input size="large" onChange={handleOnChange} placeholder="Basic usage" />

      <button
        onClick={handleAddTodo}
        className="rounded px-5 py-2 bg-red-600 text-white"
      >
        Add todo
      </button>

      <div>{renderContent()}</div>
    </div>
  );
}

export default App;
