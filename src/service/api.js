const { default: axios } = require("axios");

const https = axios.create({
  baseURL: "https://62f8b7523eab3503d1da1597.mockapi.io",
});

export const getTodos = async () => {
  let response = await https.get("/todos");
  return response.data;
};

export const addTodo = async (todo) => {
  return https.post("/todos", todo);
};
